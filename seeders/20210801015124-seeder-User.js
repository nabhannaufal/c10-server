"use strict";
const bcrypt = require("bcrypt");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Users",
      [
        {
          username: "admin",
          password: bcrypt.hashSync("admin", 10),
          email: "admin@gmail.com",
          gender: "female",
          total_score: 0,
          bio: "Ini adalah biodata dari admin",
          city: "Bogor",
          social_media_url: "http://social.admin.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "nabhan",
          password: bcrypt.hashSync("nabhan", 10),
          email: "nabhan@gmail.com",
          gender: "male",
          total_score: 0,
          bio: "Ini adalah biodata dari nabhan",
          city: "Bogor",
          social_media_url: "http://social.nabhan.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
