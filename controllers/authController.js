const { User } = require("../models");
function format(user) {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  home: (req, res) => {
    res.status(200).json({
      message: "Selamat datang di gemology API!",
      listRoute: [
        {
          login: "/api/auth/login",
          method: "post",
        },
        {
          register: "/api/auth/register",
          method: "post",
        },
        {
          profil: "/api/auth/profil",
          method: "get",
        },
      ],
    });
  },
  register: (req, res, next) => {
    User.register(req.body)
      .then(() => {
        res.json({ message: "anda berhasil daftar, silahkan login" });
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: err.message || "Error bosq, masukin data yang benar",
        });
      });
  },
  login: (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        res.json(format(user));
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: err.message || "Error bosq, masukin data yang benar",
        });
      });
  },
  profil: (req, res) => {
    const currentUser = req.user;
    res.json(currentUser);
  },
};
